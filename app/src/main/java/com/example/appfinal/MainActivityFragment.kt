package com.example.appfinal

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.fragment.app.Fragment
import com.example.appfinal.adapter.deportesadapter
import com.example.appfinal.database.ClienteInscritoDao
import com.example.appfinal.databinding.ActivityMainFragmentBinding
import com.example.appfinal.fragment.DeportesFragmen
import com.example.appfinal.fragment.FormularioInscripcion
import com.example.appfinal.model.UsuarioInscrito
import com.example.appfinal.util.Constantes
import com.example.appfinal.viewModel.CategoriaViewmodel
import kotlinx.android.synthetic.main.fragment_formulario_inscripcion.*

class MainActivityFragment : AppCompatActivity() {
    lateinit var fragment: Fragment
    lateinit var binding : ActivityMainFragmentBinding
    private val categoriaViewmodel: CategoriaViewmodel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMainFragmentBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val bundle:Bundle?=intent.extras
        bundle?.let {
            val categoria = it.getInt(Constantes.KEY_CATEGORIA)
            categoriaViewmodel.recibirCategoria(categoria)
            Toast.makeText(this,categoria.toString(),Toast.LENGTH_SHORT).show()
        }

        binding.btnPlatos.setOnClickListener {
            fragment=DeportesFragmen()
            insertarfragmento()
        }

        binding.btnMapa.setOnClickListener {
            fragment=FormularioInscripcion()
            insertarfragmento()
        }
        //setContentView(R.layout.activity_main_fragment)
        //14.10.2020
       // btnReservar.setOnClickListener {

        //}

    }

    //14.10.2020


    //11/10/2020

    //funcion insertar fragmento

    fun insertarfragmento(){
        fragment?.let { supportFragmentManager.beginTransaction().replace(R.id.contenedor,it).commit()
        }
        }


}