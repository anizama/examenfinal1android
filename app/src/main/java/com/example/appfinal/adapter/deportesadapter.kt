package com.example.appfinal.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.appfinal.R
import com.example.appfinal.databinding.ItemDeporteBinding
import com.example.appfinal.model.Deporte
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_deporte.view.*


class deportesadapter(var deportes:MutableList<Deporte> = mutableListOf()) : RecyclerView.Adapter<deportesadapter.DeportesAdapterViewHolder>()  {
        //12.10.2020
        lateinit var onItemClick:(item:Deporte)->Unit
    class DeportesAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        //12.10.2020
        var imgdeporte: ImageView=itemView.findViewById(R.id.imageView3)
        private val binding = ItemDeporteBinding.bind(itemView)

        fun bind(deporte: Deporte){

            binding.tvTitulo.text = deporte.nombreDeporte
            binding.tvDescripcion.text = deporte.descripcion
            binding.tvPrecio.text = deporte.precio.toString()
           //12.10.2020
            //Picasso.get().load(R.drawable.car1).into(itemView.imageView3)

        }

    }


    fun actualizarPlatos(deportes:MutableList<Deporte>){
        this.deportes = deportes
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeportesAdapterViewHolder {

        //view = item_plato.xml
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_deporte,parent,false)
        return DeportesAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return deportes.size
    }

    override fun onBindViewHolder(holder: DeportesAdapterViewHolder, position: Int) {

        val deporte = deportes[position]
        holder.bind(deporte)
        //11/10/2020
       holder.imgdeporte.setOnClickListener {
           //itemView=imageview3
        onItemClick(deporte)
           //Toast.makeText(this,"DISPONIBLE",Toast.LENGTH_SHORT).show()
           //println("Item Disponible")
       }

    }
}