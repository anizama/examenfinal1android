package com.example.appfinal.combobox

data class Cliente(
    val pais:String,
    val nombre:String,
    val apepat:String,
    val apemat:String,
    val iduser:String,
    val clave:String

)