package com.example.appfinal.combobox

data class ClienteInscrito (
        val valordni:String,
        val celular:String,
        val edaduser:String,
        val turno:String,
        val actividaddeportiva:String
)
