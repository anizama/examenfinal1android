package com.example.appfinal.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.appfinal.combobox.ClienteInscrito
import com.example.appfinal.fragment.FormularioInscripcion
import com.example.appfinal.model.Categoria
import com.example.appfinal.model.Deporte
import com.example.appfinal.model.Usuario
import com.example.appfinal.model.UsuarioInscrito
import com.example.appfinal.viewModel.RegistroActivity
import java.util.concurrent.Executors

@Database(entities = [Usuario::class,Deporte::class,Categoria::class,UsuarioInscrito::class],version = 1,exportSchema = true) //false cuando pase a produccion para que nadie vea.
 abstract class AppDataBase: RoomDatabase() { //AppDataBase=base de datos
    //Colocar todos los DAO
    abstract fun usuarioDao():ClienteDao
    abstract fun categoriaDao():CategoriaDao
    abstract fun deportesDao():DeportesDao
    //14.10.2020
    abstract fun clienteinscritoDao():ClienteInscritoDao
    //abstract fun clienteinscritoDao():ClienteInscritoDao

    companion object { //Acceder directamente = instancia
        private var instancia: AppDataBase? = null //bdd = instancia

        fun obtenerInstanciaBD(context:Context): AppDataBase { //Devolver la bdd si esta creada
            if (instancia == null) {
                //Crear bdd
                instancia = Room.databaseBuilder(context, AppDataBase::class.java, "bdGym")
                    .addCallback(object : RoomDatabase.Callback(){
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)
                            Executors.newSingleThreadExecutor().execute{
                                 //11/10/2020 pendiente clientesinscritos
                           //     val clienteInscrito= mutableListOf<ClienteInscrito>()
                             //   clienteInscrito.add(ClienteInscrito("74417500","989097396","25","M","Spinning"))
                               // instancia?.clienteinscritoDao()?.insertarclienteinscrito(clienteInscrito)

                                val categorias = mutableListOf<Categoria>()
                                categorias.add(Categoria(1,"MENORES DE EDAD"))
                                categorias.add(Categoria(2,"ADULTOS"))
                                instancia?.categoriaDao()?.insertarCategoria(categorias) //lista

                                val deportes= mutableListOf<Deporte>()
                                deportes.add(Deporte(1,"ClimbingKids","KIDS",100.00,"",1))
                                deportes.add(Deporte(2,"ClimbingKids","KIDS",120.00,"",1))
                                deportes.add(Deporte(3,"SwimmingAdults","ADULTS",110.00, "",2))
                                deportes.add(Deporte(4,"KarateAdults","ADULTS",130.00,"",2))
                                deportes.add(Deporte(5,"FunctionalAdults","ADULTS",120.00,"",2))
                                deportes.add(Deporte(6,"ClimbingKids","KIDS",111.11,"",1))
                                instancia?.deportesDao()?.insertardeportes(deportes)
                            }
                        }
                    }).build()

            }
            //Devuelvela
            return instancia as AppDataBase
        }

      //  fun obtenerInstanciaBD(): AppDataBase {


        //}
    }
}