package com.example.appfinal.database

import androidx.room.Dao
import androidx.room.Insert
import com.example.appfinal.model.Categoria

@Dao
interface CategoriaDao {
    @Insert
    fun insertarCategoria(categoria:MutableList<Categoria> )
}