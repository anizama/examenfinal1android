package com.example.appfinal.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.appfinal.combobox.Cliente
import com.example.appfinal.model.Usuario

@Dao
interface ClienteDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertarcliente(usuario: Usuario)

    @Query("select count(*) from tablaUsuario where iduser=:iduserInput and clave=:claveInput")
    fun autenticar(iduserInput:String, claveInput:String) : Int

}