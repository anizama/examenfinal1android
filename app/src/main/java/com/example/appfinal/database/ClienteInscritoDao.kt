package com.example.appfinal.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.appfinal.combobox.ClienteInscrito
import com.example.appfinal.model.UsuarioInscrito

@Dao
interface ClienteInscritoDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    //fun insertarclienteinscrito(usuarioInscrito: MutableList<ClienteInscrito>)
    //14.10.2020
    fun insertarclienteinscrito(usuarioinscrito:UsuarioInscrito)
    @Query("select * from tablaUsuarioInscritos")
    fun obtenerusuariosinscritos():MutableList<UsuarioInscrito>//devolver mi select de usuariosInscritos

}