package com.example.appfinal.database

import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.Query
import com.example.appfinal.model.Deporte

@Dao
interface DeportesDao {

    @Insert
    fun insertardeportes(deporte:MutableList<Deporte> )

    @Query("select *from tablaDeporte where categoria=:categoriaInput")
    fun obtenerPlatos(categoriaInput:Int) : MutableList<Deporte>
}