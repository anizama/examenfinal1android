package com.example.appfinal.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appfinal.adapter.deportesadapter
import com.example.appfinal.database.AppDataBase
import com.example.appfinal.databinding.FragmentDeportesBinding
import com.example.appfinal.model.Deporte
import com.example.appfinal.viewModel.CategoriaViewmodel
import java.util.concurrent.Executors

class DeportesFragmen:Fragment() {
    private var deportes = mutableListOf<Deporte>()
    private lateinit var binding : FragmentDeportesBinding

    private lateinit var categoriaViewmodel: CategoriaViewmodel

    private val appDatabase by lazy {
        activity?.let {
            AppDataBase.obtenerInstanciaBD(it)
        }

    }

    private val adaptador by lazy {
        deportesadapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentDeportesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.let {

            categoriaViewmodel = ViewModelProvider(it).get(CategoriaViewmodel::class.java)
        }

        binding.recyclerdeportes.layoutManager = LinearLayoutManager(context)
        binding.recyclerdeportes.adapter = adaptador

        categoriaViewmodel.categoria().observe(viewLifecycleOwner, Observer {categoria->
            appDatabase?.deportesDao()?.let {
                Executors.newSingleThreadExecutor().execute{
                    deportes=it.obtenerPlatos(categoria)
                    activity?.runOnUiThread {
                        adaptador.actualizarPlatos(deportes)


                    }

                }

            }


        })
        //12/10/2020
            adaptador.onItemClick={
              val i=Intent(Intent.ACTION_DIAL)
                i.data= Uri.parse("tel:989097396")
                startActivity(i)
            }
        }




}





