package com.example.appfinal.fragment

import com.example.appfinal.database.AppDataBase
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.appfinal.R
import com.example.appfinal.combobox.ClienteInscrito
import com.example.appfinal.databinding.ActivityMainFragmentBinding
import com.example.appfinal.databinding.FragmentFormularioInscripcionBinding
import com.example.appfinal.model.UsuarioInscrito
import kotlinx.android.synthetic.main.fragment_formulario_inscripcion.*
import java.lang.Exception
import java.util.concurrent.Executors


class FormularioInscripcion : Fragment() {
    //14.10.2020
    private lateinit var binding: FragmentFormularioInscripcionBinding
    private val appDataBase by lazy {
        activity?.let {
            AppDataBase.obtenerInstanciaBD(it)
        }

    }

    // val appDataBase:AppDataBase by lazy {
    //AppDataBase.obtenerInstanciaBD(this)
//}
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.fragment_formulario_inscripcion, container, false)
        //15.10.2020
        binding = FragmentFormularioInscripcionBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //15.10.2020

        //  activity?.let {
        //    btnReservar.setOnClickListener {
        //      registrarusuarioinscrito()
        // }
        //}
        btnReservar.setOnClickListener {
            val valordni = dni.text.toString()
            val celular = celular.text.toString()
            val edaduser = edad.text.toString()
            var turno = ""
            if (rdtgrupalman.isChecked) {
                turno = "Mañana"
            } else {
                turno = "Tarde-Noche"
            }
            var actividaddeportiva = ""
            if (ArteMarcial.isChecked) {
                actividaddeportiva = "ArtesMarciales"
            } else if (climbing.isChecked) {
                actividaddeportiva = "climbing"
            } else if (spinning.isChecked) {
                actividaddeportiva = "spinning"
            } else if (baile.isChecked) {
                actividaddeportiva = "baile"
            }

            //var turno= if (!rdtgrupalman.isChecked || !rdtgrupalnoche.isChecked) "" else   ""
            //var actividaddeportiva=if (!ArteMarcial.isChecked||!spinning.isChecked||!baile.isChecked||!climbing.isChecked) "ERROR" else ""
            //  val actividaddeportiva=""


            if (valordni.isEmpty() || celular.isEmpty() || edaduser.isEmpty()) {
                println("ERROR")
            }
            //    if(rdtgrupalnoche.isChecked||rdtgrupalman.isChecked){
            //      println("Error")}
            // if (!climbing.isChecked||!spinning.isChecked||!baile.isChecked||!ArteMarcial.isChecked){
            //    println("ERROR")
            //}
            //  val clienteInscrito=ClienteInscrito(valordni, celular, edaduser, turno, actividaddeportiva)
            // tvResultado2.text = "Estimado con numero de dni ${clienteInscrito.valordni}sera notificado al numero${clienteInscrito.celular}de edad ${clienteInscrito.edaduser}" +
            //        "años ira en el turno${clienteInscrito.turno} y escogio reservar cupo para ${clienteInscrito.actividaddeportiva}"
            //  registrarusuarioenclasegrupal()
        }
        //14.10.2020
        btnReservar.setOnClickListener {
            registrarusuarioinscrito()
        }

    }

    private fun registrarusuarioinscrito() {
        try {
            val dniclienteinscrito = dni.text.toString()
            val celularclienteinscrito = celular.text.toString()
            val edadclienteinscrito = edad.text.toString()
            var turno = ""
            var actividaddeportiva = ""
            if (rdtgrupalman.isChecked) {
                turno = "Mañana"
            } else {
                turno = "Noche"
            }
            if (ArteMarcial.isChecked) {
                actividaddeportiva = "ArteMarcial"
            } else if (climbing.isChecked) {
                actividaddeportiva = "Climbing"
            } else if (spinning.isChecked) {
                actividaddeportiva = "spinning"
            } else if (baile.isChecked) {
                actividaddeportiva = "baile"
            }
            val clienteInscrito = ClienteInscrito(dniclienteinscrito, celularclienteinscrito, edadclienteinscrito, turno, actividaddeportiva)
            tvResultado2.text = "Estimado con numero de dni ${clienteInscrito.valordni}sera notificado al numero${clienteInscrito.celular}de edad ${clienteInscrito.edaduser}" +
                    "años ira en el turno${clienteInscrito.turno.toString()} y escogio reservar cupo para ${clienteInscrito.actividaddeportiva}"
            //instanciar usuario inscrito dao
            val usuarioinsrito = UsuarioInscrito(0, dniclienteinscrito, celularclienteinscrito, edadclienteinscrito, turno, actividaddeportiva)
            Executors.newSingleThreadExecutor().execute {
                // appDataBase?.clienteInscritoDao()?.insertarclienteinscrito(usuarioinsrito)
                //  runOnUiThread{Toast.makeText(this,"Usuario grabado correctamente",Toast.LENGTH_SHORT).show()}
                appDataBase?.clienteinscritoDao()?.insertarclienteinscrito(usuarioinsrito)
            }
            } catch (ex:Exception){

            }

        }


    }




        //11/10/2020
   // private fun registrarusuarioenclasegrupal() {
            //try catch para la excepcion
           // try {
             //   val valordni = dni.text.toString()
              //  val celular = celular.text.toString()
              //  val edaduser = edad.text.toString()
               // val turno=""
               // val actividaddeportiva=""


               // val clienteInscrito = UsuarioInscrito(0,valordni,celular,edaduser,turno,actividaddeportiva)
               // Executors.newSingleThreadExecutor().execute{ appDataBase.ClienteinscritoDao().insertarclienteinscrito(clienteInscrito)
                //    runOnUiThread{Toast.makeText(this,"Usuario grabado correctamente",Toast.LENGTH_SHORT).show()}
               // }
            //}catch (ex: Exception){Toast.makeText(this,ex.toString(),Toast.LENGTH_SHORT).show()




