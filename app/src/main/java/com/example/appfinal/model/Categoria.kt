package com.example.appfinal.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tablaCategorias")
data class Categoria(

    @PrimaryKey//(autoGenerate = false)
    @NonNull
    //@ColumnInfo(name = "codigo")
    val codigo:Int ,
    @ColumnInfo(name = "descripcion")
    val descripcion:String
)