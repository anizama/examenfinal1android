package com.example.appfinal.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


    @Entity(tableName = "tablaDeporte")
    data class Deporte(

        @PrimaryKey
        @NonNull
        val codigo:Int,

        @ColumnInfo(name="nombreDeporte")
        val nombreDeporte:String,

        @ColumnInfo(name="descripcion")
        val descripcion:String,

        @ColumnInfo(name="precio")
        val precio:Double,

        @ColumnInfo(name="imagen")
        val imagen:String,

        @ColumnInfo(name="categoria")
        val categoria:Int
    )
