package com.example.appfinal.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tablaUsuario")
data class Usuario(

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "codigo")
    val codigo:Int ,

    @ColumnInfo(name = "pais")
    val pais:String,

    @ColumnInfo(name = "nombres")
    val nombres:String,

    @ColumnInfo(name = "apellidoPaterno")
    val apellidoPaterno:String,

    @ColumnInfo(name = "apellidoMaterno")
    val apellidoMaterno:String,

    @ColumnInfo(name = "iduser")
    val iduser:String,

    @ColumnInfo(name = "clave")
    val clave:String
)
