package com.example.appfinal.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tablaUsuarioInscritos")
data class UsuarioInscrito (
        @PrimaryKey(autoGenerate = true)
        @NonNull
        @ColumnInfo(name = "codigo")
        val codigoid:Int ,

        @ColumnInfo(name = "valorDNI")
        val valordni:String,

        @ColumnInfo(name = "celular")
        val celular:String,

        @ColumnInfo(name = "edaduser")
        val edaduser:String,

        @ColumnInfo(name = "turno")
        val turno:String,

        @ColumnInfo(name = "actividaddeportiva")
        val actividaddeportiva:String


)
