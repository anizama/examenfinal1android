package com.example.appfinal.util

class Constantes {

    //companion object = void
    companion object{
        const val PREFERENCIA_LOGIN: String = "PREFERENCIA_LOGIN"
        const val KEY_IDUSUARIO: String = "KEY_USUARIO"
        const val KEY_CONTRASENIA: String = "KEY_CONTRASENIA"
        const val KEY_PAIS: String = "KEY_PAIS"
        const val KEY_ESTADO_CHECKBOX: String = "KEY_ESTADO_CHECKBOX"

        const val KEY_CATEGORIA = "key_categoria"
    }

}