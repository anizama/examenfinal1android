package com.example.appfinal.util

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.appfinal.MainActivityFragment
import com.example.appfinal.R
import com.example.appfinal.databinding.ActivityLoginBinding
import com.example.appfinal.databinding.ActivityMenuBinding

class MenuActivity : AppCompatActivity() {
    lateinit var binding:ActivityMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMenuBinding.inflate(layoutInflater)
        //setContentView(R.layout.activity_menu)
        setContentView(binding.root)

        binding.btnEntradas.setOnClickListener {
            val bundle=Bundle().apply {
                putInt(Constantes.KEY_CATEGORIA,1)
            }
        val intent=Intent(this,MainActivityFragment::class.java).apply {
            putExtras(bundle)
        }
            startActivity(intent)
        }
        binding.btnSegundos.setOnClickListener {
            val bundle=Bundle().apply {
                putInt(Constantes.KEY_CATEGORIA,2)
            }
            val intent=Intent(this,MainActivityFragment::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }

    }
}