package com.example.appfinal.viewModel

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.appfinal.database.AppDataBase
import com.example.appfinal.databinding.ActivityLoginBinding
import com.example.appfinal.util.Constantes
import com.example.appfinal.util.MenuActivity
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.Executors

class LoginActivity : AppCompatActivity() {
    val flatActivo=1
    val flatNoActivo=0

    //Login Activity=ActivityLoginBinding
    lateinit var binding: ActivityLoginBinding


    private val appDatabase by lazy {
        AppDataBase.obtenerInstanciaBD(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_login)
        binding=ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        recuperarpreferencia()
        //1. Boton autenticar
        binding.btnIngresar.setOnClickListener {
            autenticar()
          /**  val idusuario = edtDNIPAIS.text.toString()
            val contrasenia = edtContrasenia.text.toString()
            var pais = ""
            if (idusuario.isEmpty()){
                Toast.makeText(this,"Debe ingresar sus credenciales",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (contrasenia.isEmpty()){
                Toast.makeText(this,"Debe ingresar sus credenciales",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!checkobl.isChecked){
                Toast.makeText(this,"Debe ingresar aceptar los terminos",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!rdtpais.isChecked ||!rdtnopais.isChecked){
                Toast.makeText(this,"Debe seleccionar un pais",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (rdtpais.isChecked){
               pais="Peru"
            }
            else{
                pais="Externo"
            }
*/
        }
         binding.btnRegistrarme.setOnClickListener {
           val intent = Intent(this,RegistroActivity::class.java)
           startActivity(intent)
       }

    }

    private fun autenticar() {
        //2. guardar funcion
        //creamos variables
        val idusuario = binding.edtDNIPAIS.text.toString()
        val contrasenia = binding.edtContrasenia.text.toString()
        var pais = ""

        if (binding.rdtpais.isChecked){
            pais="Peru"
        }
        else{
            pais="Externo"
        }
        Executors.newSingleThreadExecutor().execute{
         val respuesta=   appDatabase.usuarioDao().autenticar(idusuario,contrasenia)
            if (respuesta==0){
                runOnUiThread {
                    Toast.makeText(this,"Credenciales invalidas",Toast.LENGTH_SHORT).show()
                   // return@runOnUiThread
                }

            }else{
                //Existe el usuario
            if (checkobl.isChecked) guardarpreferencia(idusuario, contrasenia, pais, flatActivo)
                else guardarpreferencia("","","",flatNoActivo)

                irMenuPrincipal()
            }
        }

    }

    private fun irMenuPrincipal() {
        val intent=Intent(this, MenuActivity::class.java)
        startActivity(intent)
    }

    private fun guardarpreferencia(idusuario: String, contrasenia: String, pais: String, flatActivo: Int) {
            getSharedPreferences(Constantes.PREFERENCIA_LOGIN,0).edit().apply {
            putString(Constantes.KEY_IDUSUARIO,idusuario)
            putString(Constantes.KEY_CONTRASENIA,contrasenia)
            putString(Constantes.KEY_PAIS,pais)
            putInt(Constantes.KEY_ESTADO_CHECKBOX,flatActivo)
            apply() //grabar preferencia
        }

    }
        private fun recuperarpreferencia(){
            val preferencia =getSharedPreferences(Constantes.PREFERENCIA_LOGIN,0) //recuperando
            edtDNIPAIS.setText(preferencia.getString(Constantes.KEY_IDUSUARIO,""))
            edtContrasenia.setText(preferencia.getString(Constantes.KEY_CONTRASENIA,""))
            rdtpais.text = preferencia.getString(Constantes.KEY_PAIS,"")
            rdtnopais.text = preferencia.getString(Constantes.KEY_PAIS,"")

        val flatEstado = preferencia.getInt(Constantes.KEY_ESTADO_CHECKBOX,flatNoActivo)

            checkobl.isChecked = flatEstado==1
        }
}

