package com.example.appfinal.viewModel

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.appfinal.combobox.Cliente
import com.example.appfinal.combobox.IListas
import com.example.appfinal.R
import com.example.appfinal.database.AppDataBase
import com.example.appfinal.model.Usuario
import kotlinx.android.synthetic.main.activity_registro.*
import java.lang.Exception
import java.util.concurrent.Executors

class RegistroActivity : AppCompatActivity(),
    IListas {

    val appDataBase:AppDataBase by lazy { AppDataBase.obtenerInstanciaBD(this) }

    var PaisElegido = ""
    var contador = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)
        //0 Spinner real
        val pais = listOf("Peru", "Externo")
        val adapter = ArrayAdapter(this,
            R.layout.style_spinner, pais)
        spPais.adapter = adapter
        spPais.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                //Toast.makeText(this@MainActivity, languages[position], Toast.LENGTH_SHORT).show()

                if (contador!=0){
                    createLoginDialogo(pais[position]).show()
                }
                contador++

            }
            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }
        //1.
        btnRegistro.setOnClickListener {
            if (PaisElegido.isEmpty()){
                Toast.makeText(this,"Debe elegir un Pais",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            val nombre = edtNombres.text.toString()
            val apepat = edtApellidoPaterno.text.toString()
            val apemat = edtApellidoMaterno.text.toString()
            val iduser = edtCorreo.text.toString()
            val clave = edtClave.text.toString()

            val cliente = Cliente(
                PaisElegido,
                nombre,
                apepat,
                apemat,
                iduser,
                clave
            )

            Resultado.text = "Estimado ${cliente.nombre}${cliente.apepat}${cliente.apemat}.con id${cliente.iduser} de procedencia ${cliente.pais}. Corrobore su clave de acceso " +
                    "identidad que tenemos registrado ${cliente.clave}"


            registroUsuario()
        }

        //2.spinner
      /**  val spinner = findViewById<Spinner>(R.id.spinner1)
        val imageName = arrayOf("Peru", "Exterior")
        val image = intArrayOf(R.drawable.Peru, R.drawable.Exterior)
        val spinnerCustomAdapter = SpinnerCustomAdapter(applicationContext, image, imageName);
        spinner.adapter=spinnerCustomAdapter */
    }

        //1.1
    private fun registroUsuario() {
            //try catch para la excepcion
            try {
                val nombre = edtNombres.text.toString()
                val apepat = edtApellidoPaterno.text.toString()
                val apemat = edtApellidoMaterno.text.toString()
                val iduser = edtCorreo.text.toString()
                val clave = edtClave.text.toString()

                val cliente = Usuario(0,PaisElegido,nombre,apepat,apemat,iduser,clave)
                Executors.newSingleThreadExecutor().execute{  appDataBase.usuarioDao().insertarcliente(cliente)
                runOnUiThread{Toast.makeText(this,"Usuario grabado correctamente",Toast.LENGTH_SHORT).show()}
                }
            }catch (ex:Exception){Toast.makeText(this,ex.toString(),Toast.LENGTH_SHORT).show() }



    }

    /**class SpinnerCustomAdapter(internal var context: Context, internal var flags: IntArray, internal var Network: Array<String>) : BaseAdapter() { internal var inflter: LayoutInflater
        init {
            inflter = LayoutInflater.from(context)
        }
        override fun getCount(): Int {
            return flags.size
        }
        override fun getItem(i: Int): Any? {
            return null
        }
        override fun getItemId(i: Int): Long {
            return 0
        }
        override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
            var view = view
            view = inflter.inflate(R.layout.custom_spinners_items, null)
            val icon = view.findViewById(R.id.spinner_imageView) as ImageView
            val names = view.findViewById(R.id.spinner_textView) as TextView
            icon.setImageResource(flags[i])
            names.text = Network[i]
            return view
        }
    }*/
    override fun createLoginDialogo(pais:String): AlertDialog {
        val alertDialog: AlertDialog
        val builder  = AlertDialog.Builder(this)
        val inflater = layoutInflater
        val v: View = inflater.inflate(R.layout.dialogo, null)
        builder.setView(v)
        val btnAperturarNo: Button = v.findViewById(R.id.btn_aperturar_no)
        val btnAperturarSi: Button = v.findViewById(R.id.btn_aperturar_si)
        val imgLogo: ImageView = v.findViewById(R.id.imgLogo)

        if (pais == "Peru")
            imgLogo.setBackgroundResource(R.drawable.car1)
        else
            imgLogo.setBackgroundResource(R.drawable.car2)

        alertDialog = builder.create()

        btnAperturarSi.setOnClickListener {
            PaisElegido = pais
            Toast.makeText(this,"Tu nacionalidad $pais",Toast.LENGTH_SHORT).show()
            alertDialog.dismiss()
        }

        btnAperturarNo.setOnClickListener{
            alertDialog.dismiss()
        }

        return alertDialog
    }

}